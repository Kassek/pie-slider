import { terser } from 'rollup-plugin-terser';
import scss from 'rollup-plugin-scss';
import postcss from 'postcss';
import autoprefixer from 'autoprefixer';
import serve from 'rollup-plugin-serve';

export default {
    input: 'src/js/app.js',
    output: [
        {
            file: 'dist/js/script.js',
            format: 'cjs',
            sourcemap: true,
        },
        {
            file: 'dist/js/script.min.js',
            format: 'iife',
            name: 'version',
            sourcemap: true,
            plugins: [terser()]
        }
    ],
    plugins: [
        scss({
            processor: () => postcss([autoprefixer()]),
            outputStyle: 'compressed',
            output: 'dist/css/style.css',
            sass: require('node-sass'),
            watch: 'src/styles/components',
            sourceMap: true,
        }),
        serve({
            host: 'localhost',
            port: 3000,
            open: true,
            contentBase: ['dist', 'static'],
        })
    ]
};